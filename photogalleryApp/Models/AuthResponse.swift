//
//  AuthResponse.swift
//  photogalleryApp
//
//  Created by Nataly on 14.11.2021.
//

import Foundation

struct AuthResponse: Decodable {
    let accessToken: String
    let tokenType: String
    let scope: String
    let createdAt: Int
    
    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case tokenType = "token_type"
        case scope = "scope"
        case createdAt = "created_at"
    }
}

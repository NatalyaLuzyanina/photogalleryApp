//
//  Model.swift
//  photogalleryApp
//
//  Created by Nataly on 11.11.2021.
//

import Foundation

struct Result: Decodable {
    let results: [Photo]
}

struct Photo: Decodable {
    let id: String?
    let created_at: String?
    let liked_by_user: Bool
    let urls: ImageURL?
    let downloads: Int?
    let location: Location?
    let user: User?
}

struct ImageURL: Decodable {
    let regular: String?
    let thumb: String?
}

struct Location: Decodable {
    let title: String?
}

struct User: Decodable {
    let name: String?
    let username: String?
    let links: Link?
    
}

struct Link: Decodable {
    let likes: String?
}

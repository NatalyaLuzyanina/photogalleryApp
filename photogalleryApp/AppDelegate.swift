//
//  AppDelegate.swift
//  photogalleryApp
//
//  Created by Nataly on 11.11.2021.
//

import UIKit


@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let accessToken = AuthManager.shared.accessToken
        let homePage = accessToken == nil ? WelcomeViewController() : TabBarViewController()

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.rootViewController = homePage 

        return true
    }


}


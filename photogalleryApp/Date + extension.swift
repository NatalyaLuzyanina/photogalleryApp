//
//  Date + extension.swift
//  photogalleryApp
//
//  Created by Nataly on 15.11.2021.
//

import Foundation

extension Date {
    func dateFormatter(from string: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        guard let date = dateFormatter.date(from: string) else { return "" }
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"
        let datePublished = dateFormatter.string(from: date)
        return datePublished
    }
}

//
//  PhotoViewCell.swift
//  photogalleryApp
//
//  Created by Nataly on 01.12.2021.
//

import UIKit

class PhotoViewCell: UICollectionViewCell {
    
    func setCell(withPhoto photo: Photo) {
        
        let imageView = UIImageView()
        let image = ImageManager.shared.getImage(from: photo.urls?.regular ?? "")
        imageView.image = image
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(imageView)
        
        imageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        imageView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        imageView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
    }
}

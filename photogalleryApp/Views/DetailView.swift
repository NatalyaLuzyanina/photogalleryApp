//
//  DetailView.swift
//  photogalleryApp
//
//  Created by Nataly on 30.11.2021.
//

import Foundation
import UIKit

class DetailView: UIView {
    
    private var stackView = UIStackView()
    
    var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.startAnimating()
        activityIndicator.hidesWhenStopped = true
        return activityIndicator
    }()
    
    override init(frame:CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addActivityIndicator() {
        DispatchQueue.main.async {
            self.addSubview(self.activityIndicator)
            self.activityIndicator.center = self.center
        }
    }
    
    func configureViews() {
        self.addSubview(stackView)
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 10
        stackView.autoresizesSubviews = true
    }
    
    func setStackViewConstraints() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        stackView.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 20).isActive = true
        stackView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
    func addImage(withUrl url: String?) {
        let imageView = UIImageView()
        let image = ImageManager.shared.getImage(from: url ?? "")
        imageView.image = image
        let width = Float(imageView.image?.size.width ?? 0)
        let height = Float(imageView.image?.size.height ?? 0)
        
        if width > height {
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor,
                                              multiplier: CGFloat((height / width))).isActive = true
        } else {
            imageView.heightAnchor.constraint(equalToConstant: self.frame.height / 2).isActive = true
        }
        imageView.contentMode = .scaleAspectFit
        stackView.addArrangedSubview(imageView)
    }
    
    func addImageDescription(name: String, date: String, location: String, downloads: Int) {
        let nameLabel = UILabel()
        nameLabel.text = name
        nameLabel.font = UIFont.systemFont(ofSize: 18)
        
        let dateLabel = UILabel()
        let date = Date().dateFormatter(from: date)
        dateLabel.text = date
        dateLabel.font = UIFont.systemFont(ofSize: 18)
        
        let locationLabel = UILabel()
        locationLabel.text = "Location: \(location)"
        locationLabel.font = UIFont.systemFont(ofSize: 18)
        
        let downloadsLabel = UILabel()
        downloadsLabel.text = "Downloads: \(downloads)" 
        downloadsLabel.font = UIFont.systemFont(ofSize: 18)
        
        let stackLabels = UIStackView()
        stackLabels.addArrangedSubview(nameLabel)
        stackLabels.addArrangedSubview(dateLabel)
        stackLabels.addArrangedSubview(locationLabel)
        stackLabels.addArrangedSubview(downloadsLabel)
        
        stackLabels.axis = .vertical
        stackLabels.spacing = 10
        stackLabels.alignment = .leading
        stackView.addArrangedSubview(stackLabels)
    }
}

//
//  CustomView.swift
//  photogalleryApp
//
//  Created by Nataly on 30.11.2021.
//

import Foundation
import UIKit

class WelcomeView: UIView {
    
    var delegate: ButtonDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 10
        stackView.autoresizesSubviews = true
        stackView.isUserInteractionEnabled = true
        return stackView
    }()
    
    private let loginButton: UIButton = {
        let button = UIButton()
        button.isUserInteractionEnabled = true
        button.setTitle("LogIn", for: .normal)
        button.backgroundColor = .white
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(didTapLoginButton), for: .touchUpInside)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.black.cgColor
        return button
    }()
    
    private let continueButton: UIButton = {
        let button = UIButton()
        button.isUserInteractionEnabled = true
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Continue", for: .normal)
        button.backgroundColor = .white
        button.setTitleColor(.black, for: .normal)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.black.cgColor
        button.addTarget(self, action: #selector(didTapContinueButton), for: .touchUpInside)
        return button
    }()
    
    private func setupViews() {
        self.addSubview(stackView)
        stackView.addArrangedSubview(loginButton)
        stackView.addArrangedSubview(continueButton)
    }
    
    private func setupConstraints() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        stackView.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 20).isActive = true
        stackView.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: self.frame.height / 4).isActive = true
    }
    
    @objc private func didTapContinueButton() {
        delegate?.didContinueButtonTapped()
    }
    
    @objc private func didTapLoginButton() {
        delegate?.didLoginButtonTapped()
    }
}

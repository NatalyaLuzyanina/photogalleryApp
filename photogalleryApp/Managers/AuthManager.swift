//
//  AuthManager.swift
//  photogalleryApp
//
//  Created by Nataly on 13.11.2021.
//

import Foundation
import Alamofire
import KeychainSwift

class AuthManager {
    static let shared = AuthManager()
    private init(){}
    
    public let clientID = "_mS3X6iK5VMGf0XYfhKHBysUJKEwLLOxc6SHgpsyvVo"
    public let clientSecret = "R1wOcpi3vUo2Yo1-fSy0GTquNSPWqpcKbGi9K2P-2Uw"
    public let redirectUri = "https://swiftbook.ru"
    public let responseType = "code"
    public let scope = "public+write_likes"
    public let stringURL = "https://unsplash.com/oauth/authorize"
    public var isSignIn = false
    public var accessToken: String? {
        KeychainSwift().get("accessToken")
    }
    
    public func exchangeCodeForToken(code: String, completion: @escaping (Bool) -> Void) {
        guard let url = URL(string: "https://unsplash.com/oauth/token") else { return }
        
        let param = [
            "client_id": clientID,
            "client_secret": clientSecret,
            "redirect_uri": redirectUri,
            "code" : code,
            "grant_type": "authorization_code"
            
        ]
        AF.request(url, method: .post, parameters: param)
            .validate()
            .responseDecodable(of: AuthResponse.self) { dataResponse in
                switch dataResponse.result {
                case .success(let value):
                   
                    KeychainSwift().set(value.accessToken, forKey: "accessToken")
                    completion(true)
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
    }
}

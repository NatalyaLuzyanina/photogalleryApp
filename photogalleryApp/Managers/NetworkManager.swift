//
//  NetworkManager.swift
//  photogalleryApp
//
//  Created by Nataly on 11.11.2021.
//

import Foundation
import Alamofire
import AuthenticationServices

class NetworkManager {
    static let shared = NetworkManager()
    private init(){}
    
    func getPhotos(completion: @escaping ([Photo]) -> ()) {
        AF.request(
            "https://api.unsplash.com/photos",
            parameters: ["client_id": "_mS3X6iK5VMGf0XYfhKHBysUJKEwLLOxc6SHgpsyvVo"]
        )
            .validate()
            .responseDecodable(of: [Photo].self) { dataResponse in
                switch dataResponse.result {
                case .success(let photos):
                    DispatchQueue.main.async {
                        completion(photos)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                    self.showAlertIfLimitReached(dataResponse: dataResponse.response)
                }
            }
    }
    
    func fetchPhotos(query: String, completion: @escaping ([Photo]) -> ()) {
        guard let url = URL(string: "https://api.unsplash.com/search/photos") else { return }
        let parameters = [
            "query" : "\(query)",
            "client_id": "_mS3X6iK5VMGf0XYfhKHBysUJKEwLLOxc6SHgpsyvVo"
        ]
        AF.request(url, parameters: parameters)
            .validate()
            .responseDecodable(of: Result.self) { dataResponse in
                
                switch dataResponse.result {
                case .success(let results):
                    let photos = results.results
                    DispatchQueue.main.async {
                        completion(photos)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                    self.showAlertIfLimitReached(dataResponse: dataResponse.response)  
                }
            }
    }
    
    func fetchPhoto(id: String, completion: @escaping (Photo) -> ()) {
        guard let url = URL(string: "https://api.unsplash.com/photos/\(id)") else { return }
        let parameters = [
           "client_id": "_mS3X6iK5VMGf0XYfhKHBysUJKEwLLOxc6SHgpsyvVo"
        ]
        let headers: HTTPHeaders = [
            .authorization(bearerToken: AuthManager.shared.accessToken ?? "")
        ]
        AF.request(url, parameters: parameters, headers: headers)
            .validate()
            .responseDecodable(of: Photo.self) { dataResponse in
                switch dataResponse.result {
                case .success(let results):
                 completion(results)
                case .failure(let error):
                    print(error.localizedDescription)
                    self.showAlertIfLimitReached(dataResponse: dataResponse.response)
                }
            }
    }
    
    func fetchUser(completion: @escaping (User) -> ()) {

        guard let url = URL(string: "https://api.unsplash.com/me") else { return }
        guard let accessToken = AuthManager.shared.accessToken else { return }
        let parameters = [
           "client_id": "_mS3X6iK5VMGf0XYfhKHBysUJKEwLLOxc6SHgpsyvVo"
        ]
        let headers: HTTPHeaders = [
            .authorization(bearerToken: accessToken)
        ]
        AF.request(url, parameters: parameters, headers: headers)
            .validate()
            .responseDecodable(of: User.self) { dataResponse in
               
                switch dataResponse.result {
                case .success(let value):
                    completion(value)
                case .failure(let error):
                    print(error.localizedDescription)
                    self.showAlertIfLimitReached(dataResponse: dataResponse.response)
                }
            }
    }
    
    func fetchLikedPhotos(username: String, completion: @escaping ([Photo]) -> ()) {
        guard let url = URL(string: "https://api.unsplash.com//users/\(username)/likes") else { return }
        let headers: HTTPHeaders = [
            .authorization(bearerToken: AuthManager.shared.accessToken ?? "")
        ]
        AF.request(url, headers: headers)
            .validate()
            .responseDecodable(of: [Photo].self) { dataResponse in
                switch dataResponse.result {
                case .success(let value):
                    completion(value)
                case .failure(let error):
                    print(error.localizedDescription)
                    self.showAlertIfLimitReached(dataResponse: dataResponse.response)
                }
            }
    }
    
    func likePhoto(id: String) {
        guard let url =  URL(string:"https://api.unsplash.com/photos/\(id)/like") else { return }
        let headers: HTTPHeaders = [
            .authorization(bearerToken: AuthManager.shared.accessToken ?? "")
        ]
        AF.request(url, method: .post, headers: headers)
            .validate()
            .response(completionHandler: { dataResponse in
                if let error = dataResponse.error {
                    print(error.localizedDescription)
                    self.showAlertIfLimitReached(dataResponse: dataResponse.response)
                }
            })
    }
    
    func unlikePhoto(id: String) {
        guard let url =  URL(string:"https://api.unsplash.com/photos/\(id)/like") else { return }
        let headers: HTTPHeaders = [
            .authorization(bearerToken: AuthManager.shared.accessToken ?? "")
        ]
        AF.request(url, method: .delete, headers: headers)
            .validate()
            .response(completionHandler: { dataResponse in
                if let error = dataResponse.error {
                    print(error.localizedDescription)
                    self.showAlertIfLimitReached(dataResponse: dataResponse.response)
                }
            })
    }
    
    private func showAlertIfLimitReached(dataResponse: HTTPURLResponse?) {
        if let headers = dataResponse?.allHeaderFields as? [ String : Any ],
           let remainingString = headers["x-ratelimit-remaining"] as? String,
           let remaining = Int(remainingString),
           remaining == 0 {
            print(remaining)
            AlertViewController().showLimitAlert(
                withTitle: "Request limit reached",
                andMessage: "Please, try later."
            )
        }
    }
}

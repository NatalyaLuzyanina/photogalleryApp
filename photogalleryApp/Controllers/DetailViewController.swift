//
//  DetailViewController.swift
//  photogalleryApp
//
//  Created by Nataly on 12.11.2021.
//

import UIKit

class DetailViewController: UIViewController {
    
    var photoID: String?
    private var photo: Photo?
    private let detailView = DetailView()
    
    override func loadView() {
        view = detailView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        detailView.addActivityIndicator()
        setNavbar()
        fetchPhoto()
    }
    
    private func fetchPhoto() {
        guard let id = photoID else { return }
        NetworkManager.shared.fetchPhoto(id: id) { photo in
            self.photo = photo
            self.setContent()
            self.detailView.activityIndicator.stopAnimating()
        }
    }
    
    private func setNavbar() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: UIImage(systemName: "heart.fill"),
            style: .done,
            target: self,
            action: #selector(addToFavorite))
        navigationController?.navigationBar.tintColor = .gray
    }
    
    private func showAlert() {
        AlertViewController().showLoginAlert(withTitle: "Please, login", andMessage: "The list a liked photos is available to logined users") {
            self.present(AuthViewController(), animated: true)
        }
    }
    
    @objc func addToFavorite() {
        guard AuthManager.shared.isSignIn == true else {
            showAlert()
            return
        }
        guard let id = photo?.id else { return }
        guard let isFavorite = photo?.liked_by_user else { return }
        if isFavorite {
            NetworkManager.shared.unlikePhoto(id: id)
            navigationItem.rightBarButtonItem?.tintColor = .gray
        } else {
            NetworkManager.shared.likePhoto(id: id)
            navigationItem.rightBarButtonItem?.tintColor = .red
        }
    }
    
    private func setContent() {
        guard let isFavorite = photo?.liked_by_user else { return }
        navigationItem.rightBarButtonItem?.tintColor = isFavorite ? .red : .gray
        detailView.configureViews()
        detailView.addImage(withUrl: photo?.urls?.regular ?? "")
        detailView.addImageDescription(
            name: photo?.user?.name ?? "",
            date: photo?.created_at ?? "",
            location: photo?.location?.title ?? "No information",
            downloads: photo?.downloads ?? 0
        )
        detailView.setStackViewConstraints()
    }
}

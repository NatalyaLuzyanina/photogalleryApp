//
//  CollViewController.swift
//  photogalleryApp
//
//  Created by Nataly on 12.11.2021.
//

import UIKit

class PhotosViewController: UIViewController {
    
    private let itemsPerRow: CGFloat = 2
    private var photos: [Photo] = []
    private let sectionInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    private let searchController = UISearchController(searchResultsController: nil)
    private var timer: Timer?
    
    private let photosView = PhotosView()
    
    override func loadView() {
        view = photosView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        photosView.collectionView.delegate = self
        photosView.collectionView.dataSource = self
        photosView.collectionView.register(PhotoViewCell.self, forCellWithReuseIdentifier: "CellID")
        setupSearchBar()
        photosView.addActivityIndicator()
        fetchPhotos()
    }
    
    private func fetchPhotos() {
        NetworkManager.shared.getPhotos { [weak self] photos in
            self?.photos = photos
            self?.photosView.activityIndicator.stopAnimating()
            self?.photosView.collectionView.reloadData()
        }
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension PhotosViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingWidht: CGFloat = 10 * (itemsPerRow + 1)
        let availableWidth =  collectionView.frame.width - paddingWidht
        let itemWidth = availableWidth / itemsPerRow
        return CGSize(width: itemWidth, height: itemWidth)
    }
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource

extension PhotosViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellID", for: indexPath) as? PhotoViewCell else { return UICollectionViewCell() }
        
        let photo = photos[indexPath.item]
        cell.setCell(withPhoto: photo)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailvc = DetailViewController()
        detailvc.photoID = photos[indexPath.item].id
        navigationController?.pushViewController(detailvc, animated: true)
    }
}

// MARK: - UISearch

extension PhotosViewController: UISearchResultsUpdating, UISearchBarDelegate {
    
    private func setupSearchBar() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let query = searchController.searchBar.text else { return }
        
        timer?.invalidate()
        if !query.isEmpty {
            timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { _ in
                
                NetworkManager.shared.fetchPhotos(query: query) { photos in
                    self.photos = photos
                    self.photosView.activityIndicator.stopAnimating()
                    self.photosView.collectionView.reloadData()
                }
            })
        }
    }
}

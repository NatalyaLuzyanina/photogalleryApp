//
//  ViewController.swift
//  photogalleryApp
//
//  Created by Nataly on 11.11.2021.
//

import UIKit
import WebKit

class AuthViewController: UIViewController, WKNavigationDelegate {
    
    private let webView: WKWebView = {
        let prefs = WKWebpagePreferences()
        prefs.allowsContentJavaScript = true
        let config = WKWebViewConfiguration()
        config.defaultWebpagePreferences = prefs
        let webView = WKWebView(frame: .zero, configuration: config)
        return webView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.navigationDelegate = self
        view.addSubview(webView)
    
        guard let url = URL(string:
                                "https://unsplash.com/oauth/authorize?client_id=\(AuthManager.shared.clientID)&redirect_uri=\(AuthManager.shared.redirectUri)&response_type=\(AuthManager.shared.responseType)&scope=\(AuthManager.shared.scope)") else {  return }
        webView.load(URLRequest(url: url))
    
        view.backgroundColor = .systemBlue
        }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        webView.frame = view.bounds
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        guard let url = webView.url?.absoluteString,
              let code = URLComponents(string: url)?.queryItems?.first(where: { $0.name == "code"})?.value
        else { return }
        webView.isHidden = true
        
        AuthManager.shared.exchangeCodeForToken(code: code) { [weak self] success in
            DispatchQueue.main.async {
                AuthManager.shared.isSignIn = true
                let vc = TabBarViewController()
                vc.modalPresentationStyle = .fullScreen
                self?.present(vc, animated: true)
            }
        }
    }
}

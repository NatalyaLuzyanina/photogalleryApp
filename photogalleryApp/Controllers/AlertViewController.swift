//
//  AlertViewController.swift
//  photogalleryApp
//
//  Created by Nataly on 15.11.2021.
//

import UIKit

class AlertViewController: UIAlertController {
    
    private func present(alert: UIAlertController) {
        var topController = UIApplication.shared.windows.first!.rootViewController
        
        while ((topController?.presentedViewController) != nil) {
            topController = topController?.presentedViewController!
        }
        topController?.present(alert, animated: true)
        
    }
    
    func showLoginAlert(withTitle title: String, andMessage message: String, okCompletion: @escaping (() -> Void)) {
       
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { action in
            okCompletion()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive)
        
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert: alert)
    }
    
    func showLimitAlert(withTitle title: String, andMessage message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default)
        alert.addAction(okAction)
        present(alert: alert)
    }
}

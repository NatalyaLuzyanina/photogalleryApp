//
//  TableViewController.swift
//  photogalleryApp
//
//  Created by Nataly on 11.11.2021.
//

import UIKit

class TableViewController: UITableViewController {

    private var username: String?
    private var photos: [Photo]?
    private let acrivityIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "reuseIdentifier")
        AuthManager.shared.isSignIn ? fetchUser() : showAlert()
    }

    override func viewWillAppear(_ animated: Bool) {
        fetchLikedPhotos()
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        let photo = photos?[indexPath.row]
        var content = cell.defaultContentConfiguration()
        content.text = photo?.user?.name
        let imageURL = photo?.urls?.thumb
        content.image = ImageManager.shared.getImage(from: imageURL ?? "")
        
        cell.contentConfiguration = content
        return cell
    }
    
    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailvc = DetailViewController()
        detailvc.photoID = photos?[indexPath.row].id
        navigationController?.pushViewController(detailvc, animated: true)
    }
    
    // MARK: - Private methods
    
    private func fetchUser() {
        NetworkManager.shared.fetchUser { [unowned self] user in
            DispatchQueue.main.async {
                self.username = user.username
                self.fetchLikedPhotos()
            }
        }
    }
    
    private func fetchLikedPhotos() {
        guard let username = self.username else { return }
        NetworkManager.shared.fetchLikedPhotos(username: username) { [unowned self] photos in
            self.photos = photos
            self.tableView.reloadData()
        }
    }
    
    private func showAlert() {
        AlertViewController().showLoginAlert(withTitle: "Please, login", andMessage: "The list a liked photos is available to logined users") {
            self.present(AuthViewController(), animated: true)
        }
    }
}

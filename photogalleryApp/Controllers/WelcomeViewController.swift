//
//  LoginViewController.swift
//  photogalleryApp
//
//  Created by Nataly on 14.11.2021.
//

import UIKit

protocol ButtonDelegate {
    func didLoginButtonTapped()
    func didContinueButtonTapped()
}

class WelcomeViewController: UIViewController {
    
    let welcomeView = WelcomeView()
    
    override func loadView() {
        view = welcomeView
        welcomeView.delegate = self
    }
}

extension WelcomeViewController: ButtonDelegate {
    func didLoginButtonTapped() {
        let vc = AuthViewController()
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true)
    }
    
    func didContinueButtonTapped() {
        let tabBar = TabBarViewController()
        tabBar.modalPresentationStyle = .fullScreen
        present(tabBar, animated: true)
    }
}

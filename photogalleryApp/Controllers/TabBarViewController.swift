//
//  TabBarViewController.swift
//  photogalleryApp
//
//  Created by Nataly on 11.11.2021.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tableVC = TableViewController()
        tableVC.tabBarItem.image = UIImage(systemName: "heart.fill")
        tabBar.tintColor = .black
        let collVC = PhotosViewController()
        collVC.tabBarItem.image = UIImage(systemName: "photo.fill.on.rectangle.fill")
        let navigationCollVC = UINavigationController(rootViewController: collVC)
        let navigationTableVC = UINavigationController(rootViewController: tableVC)
        
        viewControllers = [navigationCollVC, navigationTableVC]
    }
}
